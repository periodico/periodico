<?php
include_once(dirname(__FILE__).'/app/app.php');
include_once(dirname(__FILE__).'/app/lib/Cache.php');

include(dirname(__FILE__).'/app/lib/striptags/allow_some_html_tags.php');

if ($Planet->loadOpml(dirname(__FILE__).'/custom/people.opml') == 0) exit;

$Planet->loadFeeds();
$items = $Planet->getItems();
$limit = $PlanetConfig->getMaxDisplay();
$count = 0;

header('Content-Type: application/atom+xml; charset=UTF-8');
echo '<?xml version="1.0" encoding="UTF-8" ?>';
?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <title><?=htmlspecialchars($PlanetConfig->getName())?></title>
    <subtitle><?=htmlspecialchars($PlanetConfig->getName())?></subtitle>
    <id><?=$PlanetConfig->getUrl()?></id>
    <link rel="self" type="application/atom+xml" href="<?=$PlanetConfig->getUrl()?>atom.php" />
    <link rel="alternate" type="text/html" href="<?=$PlanetConfig->getUrl()?>" />
    <icon>images/periodico_light.png</icon>
    <logo>images/periodico_light.png</logo>
    <updated><?=date("Y-m-d\TH:i:s\Z")?></updated>
    <author><name><?=htmlspecialchars($PlanetConfig->getName())?></name></author>

<?php $count = 0; ?>
<?php foreach ($items as $item): ?>

    <entry>
        <title type="html"><?=htmlspecialchars($item->get_feed()->getName())?>: <?=htmlspecialchars(strip_tags($item->get_title()))?></title>
        <id><?=htmlspecialchars($item->get_permalink())?></id>
        <link rel="alternate" href="<?=htmlspecialchars($item->get_permalink())?>"/>
        <published><?=$item->get_date('Y-m-d\\TH:i:s+00:00')?></published>
        <updated><?=$item->get_date('Y-m-d\\TH:i:s+00:00')?></updated>
        <author><name><?=($item->get_author() ? $item->get_author()->get_name() : 'anonymous')?></name></author>

        <content type="html"><![CDATA[<?php
                                $removeTags = new allow_some_html_tags();
                                $removeTags->setAllowed( array("p", "span", "ul", "li", "br", "img"), array("src", "href"));
                                $removeTags->loadHTML(substr($item->get_content(), 0, 800));
                                $resumeAux = $removeTags->getCleanHTML();

                                $lastSpace = strrpos($resumeAux," ");
                                
                                echo substr($resumeAux, 0, $lastSpace) . '... <p/>[Texto completo em ' . $item->get_permalink() 
                                    . ']';
                                      ?>]]></content>
    </entry>

<?php if (++$count == $limit) break; ?>
<?php endforeach; ?>

</feed>
