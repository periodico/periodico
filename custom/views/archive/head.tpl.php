    <link rel="stylesheet" media="screen" type="text/css" href="custom/style/periodico.css" title="Periódico" />

    <link rel="alternate" type="application/atom+xml" title="ATOM" href="atom.php" />
    <script type="text/javascript">
        function toggle(id) {
            var e = document.getElementById(id);
            var top_info = document.getElementById("top_info");
            
            if(e.style.display == 'inline-block') {
                e.style.display = 'none';
                top_info.style.display = 'none';
            } else {
                document.getElementById("about").style.display = 'none';
                document.getElementById("authors").style.display = 'none';
                e.style.display = 'inline-block';
                top_info.style.display = 'inline-block';
            }
        }
    </script>
