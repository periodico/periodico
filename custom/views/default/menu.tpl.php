<div style="width: 100%; text-align: center;">
    <div style="display: inline-block; width:500px;margin-bottom:10px">
        <div style="display: inline-block;">
            <table>
                <tr>
                    <td>
                        <div class="top_button">
                            <img class="icon" src="custom/img/about.png" style="width:50px;height:50px"/>
                            <br/>
                            <a class="top_button" href="javascript:toggle('about')">Sobre</a>
                        </div>
                    </td>
                    <td>
                        <div class="top_button">
                            <img class="icon" src="custom/img/authors.png" style="width:50px;height:50px" />
                            <br/>
                            <a class="top_button" href="javascript:toggle('authors')">Autores</a>
                       </div>
                    </td>
                    <td>
                        <div class="top_button">
                            <img class="icon" src="custom/img/archive.png" style="width:50px;height:50px" />
                            <br/>
                            <a class="top_button" href="?type=archive">Arquivos</a>
                       </div>
                    </td>
                    <td>
                        <div class="top_button">
                            <img class="icon" src="custom/img/rss.png" style="width:50px;height:50px" />
                            <br/>
                            <a class="top_button" href="atom.php">Feed</a>
                       </div>
                    </td>
                </tr>
            </table>
        </div>
    </div> 
</div>

<div style="display: inline-block;">
    <div id="top_info" style="display: none; margin-bottom:10px">
    
        <div id="about" class="menu-item" style="display:none">
            <br>
            <h2 style="color: #0489B7;">Sobre</h2>
            <p>O <b>Periódico</b> (ou <i>Journal of the Brazilian Scientific Blogs Community - JBSBC</i>) é um agregador de
            conteúdo da comunidade de blogs de divulgação científica do país. O site é alimentado a partir dos <i>feeds</i> de
            cada blog agregado.</p>
            <p>O objetivo do <b>Periódico</b> é agregar links para os diversos posts produzidos pela comunidade brasileira de
            blogs de divulgação científica, arquivá-los e espalhá-los via redes sociais. Também objetivamos tornar mais
            simples encontrar blogs científicos em português e divulgar novos blogs na comunidade.</p>
            <h2 style="color: #0489B7;">Contatos e Redes Sociais</h2>
            <p>Nosso twitter é <a href="https://twitter.com/periodicobr">@periodicobr</a>.</p>
            <p>Nosso facebook é <a href="https://www.facebook.com/periodicobr">periodicobr</a>.</p>
            <p>Para entrar em contato, mande e-mail para <a href="mailto:contato@periodico.blog.br">
            contato@periodico.blog.br</a></p>
            <h2 style="color: #0489B7;">Adicionar meu blog</h2>
            <p>Caso você queira adicionar seu blog no <b>Periódico</b>, envie e-mail para <a
            href="mailto:contato@periodico.blog.br">contato@periodico.blog.br</a> contendo:</p>
            <ul>
            <li>Nome do blog;</li>
            <li>Endereço do blog;</li>
            <li>Endereço do <i>feed</i> a ser agregado.</li>
            </ul>
            <p>Caso seu blog não seja exclusivamente sobre ciência, você pode criar uma tag específica e passar o <i>feed</i>
            dessa tag para ser agregado.</p>
            <h2 style="color: #0489B7;">Software</h2>
            <p><b>Periódico</b> roda <a href="http://moonmoon.org/">moonmoon</a>, um software do tipo <a
            href="http://en.wikipedia.org/wiki/Planet_%28software%29">planet</a>, muito utilizado em comunidades de software
            livre. Os ícones vem do projeto <a href="http://www.oxygen-icons.org/">Oxygen</a>, parte do <a
            href="http://kde.org/">KDE</a>.</p>
            <p>Os bots que alimentam as redes sociais a partir do feed do <b>Periódico</b> são fornecidos pela <a
            href="https://ifttt.com/">IFTTT</a>.</p>
            <p>O código-fonte do <b>Periódico</b> está <a href="https://gitlab.com/periodico/periodico">disponível</a> em um repositório
            público, sob licença AGPLv3.</p>
            <h2 style="color: #0489B7;">Compromisso</h2>
            <p>O mantenedor do <b>Periódico</b>, <a href="http://filipesaraiva.info/">Filipe Saraiva</a>, é um fã dos blogs
            científicos e acompanha alguns deles há bastante tempo. A intenção de criar o site é apenas auxiliar na divulgação
            deste tipo de blog e suas produções.</p>
            <p>Não há qualquer intenção comercial com o <b>Periódico</b>, nem qualquer desejo de "posse" sobre a produção dos
            blogueiros. <b>Periódico</b> nunca exibirá propaganda ou publicidade de qualquer tipo. E caso o site, por algum
            motivo, se mostre desinteressante à comunidade, ele será retirado do ar.</p>
            <p>Se algum dia o mantenedor do <b>Periódico</b> não puder mais exercer este trabalho, tudo que for relacionado ao
            site, desde o código-fonte até a URL, passando pelos perfis das redes sociais, serão disponibilizados para que
            outros possam dar continuidade à manutenção.</p>
            <p>O código-fonte do <b>Periódico</b> está <a href="https://gitlab.com/periodico/periodico">livremente disponível</a>
            na internet sob licença AGLPv3.</p>
        </div>
        
        <div id="authors" class="menu-item" style="display:none">
            <br>
            <?php
                $all_people = &$Planet->getPeople();
                usort($all_people, array('PlanetFeed', 'compare'));
                $cont_people = count($all_people);
                
                $cont_people_per_column = 1;
                $people_per_column = 3;
                
            ?>
            <h2 style="color: #0489B7;">Autores<?php echo ' (' . $cont_people . ')'?></h2>
            
            <table style="border-collapse: separate; border-spacing: 38.9px 10px;">
            <?php foreach ($all_people as $person) : ?>
                
                <?php if($cont_people_per_column == 1): ?>
                    <tr>
                <?php endif; ?>
                
                <td>
                    <a href="<?php echo htmlspecialchars($person->getFeed(), ENT_QUOTES, 'UTF-8'); ?>" title="<?=_g('Feed')?>"><img src="postload.php?url=<?php echo urlencode(htmlspecialchars($person->getFeed(), ENT_QUOTES, 'UTF-8')); ?>" alt="" height="12" width="12" /></a>
                    <a href="<?php echo $person->getWebsite(); ?>" title="<?=_g('Website')?>"><?php echo htmlspecialchars($person->getName(), ENT_QUOTES, 'UTF-8'); ?></a>
                </td>
                
                <?php if($cont_people_per_column == $people_per_column): ?>
                    </tr>
                    <?php $cont_people_per_column = 0; ?>
                <?php endif; ?>
                
                <?php $cont_people_per_column += 1; ?>
            <?php endforeach; ?>
            </table>
            
            <p>
            <img src="custom/img/opml.png" alt="<?=_g('Feed')?>" height="12" width="12" /> <a href="custom/people.opml"><?=_g('All feeds in OPML format')?></a></p>
            <h2 style="color: #0489B7;">Adicionar meu blog</h2>
            <p>Caso você queira adicionar seu blog no <b>Periódico</b>, envie e-mail para <a
            href="mailto:contato@periodico.blog.br">contato@periodico.blog.br</a> contendo:</p>
            <ul>
            <li>Nome do blog;</li>
            <li>Endereço do blog;</li>
            <li>Endereço do <i>feed</i> a ser agregado.</li>
            </ul>
            <p>Caso seu blog não seja exclusivamente sobre ciência, você pode criar uma tag específica e passar o <i>feed</i>
            dessa tag para ser agregado.</p>            
        </div>
        
    </div>
</div>
